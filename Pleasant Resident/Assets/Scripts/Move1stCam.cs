﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move1stCam : MonoBehaviour {
    public float speed,length,moveSpeed;
    public bool doIt,move;
    public GameObject movePoint;
	// Use this for initialization
	void Start () {
        StartCoroutine(RotateIt());
	}
	
	// Update is called once per frame
	void Update () {
        if (doIt)
        {
            transform.Rotate(speed * Time.deltaTime, 0, 0);
        }
        if (move)
        {
            transform.position = Vector3.MoveTowards(transform.position, movePoint.transform.position, moveSpeed);
            if (Vector3.Distance(transform.position, movePoint.transform.position) <= .1f)
            {
                move = false;
            }
        }
        
    }
    IEnumerator RotateIt()
    {
        doIt = true;
        yield return new  WaitForSeconds(length);
        doIt = false;
        move = true;
        yield break;
        
    }
}
