﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTileOffsetY : MonoBehaviour {
    public Material mat;
    public float scrollSpeed;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        mat.SetTextureOffset("_MainTex", new Vector2(0,Time.time*scrollSpeed));
	}
}
