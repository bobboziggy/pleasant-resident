﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomColorScript : MonoBehaviour {
    Renderer rend;
    public Material mat;

	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
        mat = rend.material;
        mat.EnableKeyword("_EMISSION");
	}
	
	// Update is called once per frame
	void Update () {
        mat.SetColor("_EmissionColor", Color.blue);
	}
}
