﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ResidentMove : MonoBehaviour {

    public static ResidentMove RM;
    Rigidbody rb;

    [Header("Vector3 Vars")]
    Animator doorAnim;
    Animator floorAnim;
    public Vector3 rayStart;
    public Vector3 rayDirection;

    [Header("Float Vars")]
    public float eAngleX;
    public float eAngleY, eAngleZ, speed, maxDownVel, gravityDown, gravityUp, velGreaterThanNum, rotateSpeed, aimRotateSpeed, aimVertSpeed, rayHeight, pistolRayLength,
        pistolRayRadius, sRayLength, sRayRadius, camNum, maxVertRotate1, maxVertRotate2, pistolAmmo, pistolAmmoAmt, shotgunAmmo, shotgunAmmoAmt, playerHP, playerHPMax,
        pistolDmg, shotgunDmg, NPCRotateTowardSpeed, doorOpenTimer, doorOpenTimerMax, pickupTextLength, keyCount,colliderDisabledTimer;

    [Header("Bool Vars")]
    public bool onTopOfPlatform;
    public bool hasGun, canInteract, talking, hasPistol, hasShotgun, aiming, doorOpenBool, closeDoor, letPlayerIn, korbusTalk, korpusTalk, pastIntro, gotAKey;
    //public TextMeshProUGUI interactText, ammoText;

    [Header("Cam Vars")]
    public List<Camera> cam;
    public List<GameObject> camZone;
    public Camera camToPass;

    [Header("General GO Vars")]
    public GameObject spawnPoint;
    public GameObject groundGun, ammo,  gun,  trashWorld, straightAhead,straightBehind, roomForEnemy,pistolPrefab, pistolInstant, shotgunPrefab, shotgunInstant, 
        interactObjToPass, f1End, f2Start;
    public List<GameObject> doors;
    public List<Rigidbody> allRBs;
    RaycastHit hitInfo;
    public List<GameObject> floorTriggers,floors;
    public GameObject floorToPass;

    [Header("Dialogues")]
    public List<Dialogue> NPCDiags;
    public Dialogue lockedDiag;
   

    private void Awake()
    {
        if (RM == null)
        {
            RM = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start () {
        rb = GetComponent<Rigidbody>();
        cam[0].enabled = true;
        UIManager.UIM.interactText.text = "PRESS SPACE TO INTERACT";
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetButtonDown("Jump"))
        {
            print("spacebar");
        }
        eAngleX = transform.eulerAngles.x;      
        eAngleY = transform.eulerAngles.y;
        eAngleZ = transform.eulerAngles.z;
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
        AlwaysCheckingBools();

      
        if (playerHP <= 0)
        {
            //destroy player and spawn newplayer (MAKE PLAYER A PREFAB FIRST)
            transform.position = spawnPoint.transform.position;
           
            rb.constraints = RigidbodyConstraints.FreezeAll;
            rb.constraints = RigidbodyConstraints.None;
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            //rb.constraints = RigidbodyConstraints.FreezeRotationZ;
            playerHP = playerHPMax;
        }

        
    }
    private void FixedUpdate()
    {
        AlwaysCheckingMovement();
    }



    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Door"))
        {
            //other.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;

        }
        if (other.transform.CompareTag("Respawn"))
        {
            transform.position = spawnPoint.transform.position;
            
        }
        if (other.transform.CompareTag("Target"))
        {
            other.collider.GetComponent<EnemyMove>().DealDamage(other.collider.GetComponent<EnemyMove>().damage);
            print(playerHP);
        }

    }
    private void OnCollisionStay(Collision other)
    {
        if (other.transform.CompareTag("Target"))
        {
            other.collider.GetComponent<EnemyMove>().DealDamage(other.collider.GetComponent<EnemyMove>().damage);
            print(playerHP);
        }
    }
    private void OnCollisionExit(Collision other)
    {
        if (other.transform.CompareTag("Door"))
        {
            //other.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
       
    }
    private void OnTriggerEnter(Collider other) // camera zone / cam select
    {
        if (other.transform.CompareTag("Floor"))
        {
            for (int i = 0; i < floorTriggers.Count; i++)
            {

                if (other.transform.gameObject == floorTriggers[i])
                {
                    print(other.transform.parent.transform.parent.name);
                    floorToPass = other.transform.parent.gameObject;
                    SwitchFloor(floorToPass);
                }
                else
                {

                }
            }
        }
        if (other.transform.CompareTag("Cam"))
        {
            for(int i = 0; i < camZone.Count; i++)
            {
                
                if (other.transform.gameObject == camZone[i])
                {
                    roomForEnemy = camZone[i];
                    camToPass = cam[i];
                    print(camToPass.name);
                    SwitchCam(camToPass);
                }
                else
                {

                }
            }
        }
       
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.CompareTag("InteractZone"))
        {
            canInteract = true;
            print("inzone");
            if (other.GetComponentInParent<Interactable>() != null)
            {
                if(other.GetComponentInParent<Interactable>().type == "Gun")
                {
                    interactObjToPass = other.transform.parent.gameObject;
                    GunsInteract(interactObjToPass);
                }
                if (other.GetComponentInParent<Interactable>().type == "Ammo")
                {
                    interactObjToPass = other.transform.parent.gameObject;
                    AmmoInteract(interactObjToPass);
                }
                if (other.GetComponentInParent<Interactable>().type == "NPC")
                {
                    interactObjToPass = other.transform.parent.gameObject;
                    NPCInteract(interactObjToPass);
                }
                if (other.GetComponentInParent<Interactable>().type == "Door")
                {
                    interactObjToPass = other.transform.parent.gameObject;
                    StartCoroutine(DoorInteract(interactObjToPass));
                    //DoorInteract(interactObjToPass);
                }
                if (other.GetComponentInParent<Interactable>().type == "LockedDoor")
                {
                    interactObjToPass = other.transform.parent.gameObject;
                    LockedDoorInteract(interactObjToPass);
                }
                if (other.GetComponentInParent<Interactable>().type == "Item")
                {
                    interactObjToPass = other.transform.parent.gameObject;
                    ItemInteract(interactObjToPass);
                }
            }
            else
            {

            }
            
        }
        else
        {
            
        }
       
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("InteractZone"))
        {
            canInteract = false;
        }
    }



    public void PistolRay()
    {
        rayStart = transform.position + new Vector3(0, rayHeight, 0);
        rayDirection = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(rayStart, rayDirection * pistolRayLength, Color.red);
        Physics.SphereCast(rayStart, pistolRayRadius, rayDirection, out hitInfo, pistolRayLength);
    }
    public void ShotgunRay()
    {
        rayStart = transform.position + new Vector3(0, rayHeight, 0);
        rayDirection = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(rayStart, rayDirection * sRayLength, Color.blue);
        Physics.SphereCast(rayStart, sRayRadius, rayDirection, out hitInfo, sRayLength);
    }
    private void OnDrawGizmos()
    {
        if (hasShotgun)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(rayStart + rayDirection * sRayLength, sRayRadius);
        }
        if (hasPistol)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(rayStart + rayDirection * pistolRayLength, pistolRayRadius);
        }
      
    }
    public void ShootPistol()
    {
        pistolAmmo--;
        if (hitInfo.collider != null)
        {
      
        }
        if (hitInfo.collider != null && hitInfo.collider.CompareTag("Target"))
        {
            hitInfo.collider.gameObject.GetComponent<EnemyMove>().TakeDamage(pistolDmg);
            //Destroy(hitInfo.collider.gameObject);
            print("hitting target");
        }
        else
        {

        }
    }
    public void ShootShotgun()
    {
        shotgunAmmo--;
        if (hitInfo.collider != null)
        {

        }
        if (hitInfo.collider != null && hitInfo.collider.CompareTag("Target"))
        {
            hitInfo.collider.gameObject.GetComponent<EnemyMove>().TakeDamage(shotgunDmg);
            //Destroy(hitInfo.collider.gameObject);
            print("hitting target");
        }
        else
        {

        }
    }
    public void SwitchFloor(GameObject daFloor)
    {
        foreach(GameObject element in floors)
        {
           element.GetComponent<Animator>().enabled = false;
        }
        floorAnim = daFloor.GetComponent<Animator>();
        floorAnim.enabled = true;
    }
    public void SwitchCam(Camera camYouWant)
    {
        foreach (Camera element in cam)
        {
            element.enabled = false;
        }
        camYouWant.enabled = true;

        //tell the enemies to check themselves before they wreck themselves.
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Target"))
        {
            g.GetComponent<EnemyMove>().ChasePlayer();
        }
    }
    public void GunsInteract(GameObject theGun)
    {
        UIManager.UIM.interactText.text = "[SPACE] PICK UP " + theGun.tag.ToUpper();
        if (Input.GetButtonDown("Jump"))
        {
            print(theGun.name + theGun.tag);
            if(theGun.tag == "Pistol")
            {
                Destroy(theGun);
                pistolInstant = Instantiate(pistolPrefab, gun.transform.position, Quaternion.identity);
                pistolInstant.transform.SetParent(gun.transform);
                hasPistol = true;
                hasShotgun = false;
                canInteract = false;
                UIManager.UIM.pickupText.text = "GOT " + theGun.tag.ToUpper() + "!";
                StartCoroutine(ShowPickup());
            }
            if (theGun.tag == "Shotgun")
            {
                Destroy(theGun);
                shotgunInstant = Instantiate(shotgunPrefab, gun.transform.position, Quaternion.identity);
                shotgunInstant.transform.SetParent(gun.transform);
                hasPistol = false;
                hasShotgun = true;
                canInteract = false;
                UIManager.UIM.pickupText.text = "GOT " + theGun.tag.ToUpper() + "!";
                StartCoroutine(ShowPickup());
            }
        }
    }
    public void AmmoInteract(GameObject theAmmo)
    {
        UIManager.UIM.interactText.text = "[SPACE] PICK UP " + theAmmo.tag.ToUpper() + " AMMO";
        if (Input.GetButtonDown("Jump"))
        {
            if (theAmmo.tag == "Pistol")
            {
                Destroy(theAmmo);
                pistolAmmo = pistolAmmo + pistolAmmoAmt;
                canInteract = false;
                UIManager.UIM.pickupText.text = "GOT " + pistolAmmoAmt.ToString() + " " + theAmmo.tag.ToUpper() + " AMMO!";
                StartCoroutine(ShowPickup());
                if (!hasPistol)
                {
                    StartCoroutine(ShowAmmo());
                }
            }
            if (theAmmo.tag == "Shotgun")
            {
                Destroy(theAmmo);
                shotgunAmmo = shotgunAmmo + shotgunAmmoAmt;
                canInteract = false;
                UIManager.UIM.pickupText.text = "GOT " + shotgunAmmoAmt.ToString() +" "+ theAmmo.tag.ToUpper() + " AMMO!";
                StartCoroutine(ShowPickup());
                if (!hasShotgun)
                {
                    StartCoroutine(ShowAmmo());
                }
            }

        }
    }
    public void NPCInteract(GameObject theNPC)
    {
        UIManager.UIM.interactText.text = "[SPACE] TALK";
        if(theNPC.tag == "NPC2" && DialogueManager.DM.inDialogue == false && pastIntro == false)
        {
            DialogueManager.DM.StartDialogue(NPCDiags[1]);
            pastIntro = true;
        }
        if (Input.GetButtonDown("Jump") && DialogueManager.DM.inDialogue == false)
        {

          
            Vector3 targetPostition = new Vector3(theNPC.transform.position.x, transform.position.y, theNPC.transform.position.z);
            transform.LookAt(Vector3.Lerp(transform.position, targetPostition, aimVertSpeed));

           
            for(int i = 1; i <= NPCDiags.Count; i++)
            {
                if(theNPC.tag == "NPC" + i.ToString())
                {
                    if (NPCDiags[i-1].NPCName == "Korbus")
                    {
                        korbusTalk = true;
                    }
                    if (NPCDiags[i-1].NPCName == "Korpus")
                    {
                        korpusTalk = true;
                    }
                    DialogueManager.DM.StartDialogue(NPCDiags[i-1]);
                    
                }
                else
                {

                }
                
            }
            
        }
    }
    
   
    public IEnumerator DoorInteract(GameObject theDoor)
    {
        Animator dAnim = theDoor.GetComponent<Animator>();
        if (dAnim.GetBool("openingDoor")==false)
        {
            UIManager.UIM.interactText.text = "[SPACE] OPEN DOOR";
        }
        if (dAnim.GetBool("openingDoor") == true)
        {
            UIManager.UIM.interactText.text = "";
        }
        if (Input.GetButtonDown("Jump"))

        {
            dAnim.SetBool("openingDoor", true);

            dAnim.SetBool("closingDoor", false);
            yield return new WaitForSeconds(colliderDisabledTimer);
            theDoor.GetComponent<Collider>().enabled = false;

            yield return new WaitForSeconds(doorOpenTimer);

            dAnim.SetBool("closingDoor", true);

            dAnim.SetBool("openingDoor", false);
            yield return new WaitForSeconds(colliderDisabledTimer);
            theDoor.GetComponent<Collider>().enabled = true;

        }
    }
    public void LockedDoorInteract(GameObject lockedDoor)
    {
        if (keyCount <= 0)
        {
            UIManager.UIM.interactText.text = "[SPACE] OPEN DOOR";
        }
        if(keyCount > 0)
        {
            UIManager.UIM.interactText.text = "[SPACE] USE KEY";
        }
        if (Input.GetButtonDown("Jump") && !DialogueManager.DM.inDialogue)
        {
            if (keyCount <= 0)
            {
                DialogueManager.DM.StartDialogue(lockedDiag);
            }
            if (keyCount > 0)
            {
                keyCount--;
                lockedDoor.GetComponent<Interactable>().type = "Door";
            }
        }
        
    }
    public void ItemInteract(GameObject theItem)
    {
        UIManager.UIM.interactText.text = "[SPACE] PICK UP " + theItem.tag.ToUpper();
        if (Input.GetButtonDown("Jump"))
        {
            if (theItem.tag == "Key")
            {
                gotAKey = true;
                keyCount++;
                Destroy(theItem);
                canInteract = false;
               
            }
            UIManager.UIM.pickupText.text = "GOT " + theItem.tag.ToUpper() + "!";
        }
          
    }
    public IEnumerator ShowAmmo()
    {
        UIManager.UIM.ammoText.enabled = true;
        yield return new WaitForSeconds(pickupTextLength);
        UIManager.UIM.ammoText.enabled = false;
    }
    public IEnumerator ShowPickup()
    {
        UIManager.UIM.pickupText.enabled = true;
        yield return new WaitForSeconds(pickupTextLength);
        UIManager.UIM.pickupText.enabled = false;
    }
    public void KorpY()
    {
        StartCoroutine(KorpYes());
    }
    public void KorbY()
    {
        StartCoroutine(KorbYes());
    }
    public IEnumerator KorpYes()
    {
        korpusTalk = false;
        DialogueManager.DM.DisplayNextSentence();
        yield return new WaitUntil(() => DialogueManager.DM.inDialogue == false);
        transform.position = f1End.transform.position; //make lerpy later
    }
    public void KorpN()
    {
        korpusTalk = false;
        DialogueManager.DM.DisplayNextSentence();
    }
    public IEnumerator KorbYes()
    {
        korbusTalk = false;
        DialogueManager.DM.DisplayNextSentence();
        yield return new WaitUntil(() => DialogueManager.DM.inDialogue == false);
        transform.position = f2Start.transform.position; //make lerpy later
    }
    public void KorbN()
    {
        korbusTalk = false;
        DialogueManager.DM.DisplayNextSentence();
    }
    public void AlwaysCheckingMovement()
    {
        if (!aiming && !DialogueManager.DM.inDialogue)
        {

            //velocity.z = Input.GetAxisRaw("Vertical") * speed; // movement start
            Vector3 rotateAmount = new Vector3(0, rotateSpeed * Input.GetAxisRaw("Horizontal") * Time.deltaTime, 0); // rotates
            transform.Rotate(rotateAmount);
            if (Input.GetAxisRaw("Vertical") > 0)
            {
                rb.MovePosition(Vector3.MoveTowards(transform.position, straightAhead.transform.position, speed * Time.deltaTime));
                //rb.MovePosition(transform.position + velocity *Time.deltaTime);
            }
            if (Input.GetAxisRaw("Vertical") < 0)
            {
                rb.MovePosition(Vector3.MoveTowards(transform.position, straightBehind.transform.position, speed * Time.deltaTime));
                //rb.MovePosition(transform.position + velocity * Time.deltaTime);
            } // movement end

        }
        if (aiming && !DialogueManager.DM.inDialogue)
        {
            Vector3 rotateAmount = new Vector3(Input.GetAxisRaw("Vertical") * aimVertSpeed * Time.deltaTime, aimRotateSpeed * Input.GetAxisRaw("Horizontal") * Time.deltaTime, 0); // rotates

            //rotateAmount.x = Mathf.Clamp(rotateAmount.x, minRot, maxRot);

            if (Input.GetAxisRaw("Vertical") > .5)

            {

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(maxVertRotate1, eAngleY, 0)), aimVertSpeed);

                //transform.rotation = Quaternion.Euler(new Vector3(maxRot, eAngleY, 0));

            }
            else if (Input.GetAxisRaw("Vertical") < -.5)

            {

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(maxVertRotate2, eAngleY, 0)), aimVertSpeed);

                //transform.rotation = Quaternion.Euler(new Vector3(minRot, eAngleY, 0));

            }

            else

            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, eAngleY, 0)), aimVertSpeed);
                transform.Rotate(rotateAmount);

            }
            
            if (DialogueManager.DM.inDialogue)
            {

            }

        }
    }
   
    public void AlwaysCheckingBools()
    {
        if(gotAKey == true)
        {
            UIManager.UIM.keyText.text = "Keys: " + keyCount;
            UIManager.UIM.keyText.enabled = true;
        }
        if(korbusTalk == true)
        {
            UIManager.UIM.korbYes.gameObject.SetActive(true);
           
            UIManager.UIM.korbNo.gameObject.SetActive(true);
           
        }
        if (korpusTalk == true)
        {
            UIManager.UIM.korpYes.gameObject.SetActive(true);
            UIManager.UIM.korpNo.gameObject.SetActive(true);
            
        }
        if(korbusTalk == false && korpusTalk == false)
        {
            UIManager.UIM.korbYes.gameObject.SetActive(false);
            UIManager.UIM.korbNo.gameObject.SetActive(false);
            UIManager.UIM.korpYes.gameObject.SetActive(false);
            UIManager.UIM.korpNo.gameObject.SetActive(false);
        }

        if (canInteract == true)
        {
            UIManager.UIM.interactText.enabled = true;

        }
        if (canInteract == false)
        {
            UIManager.UIM.interactText.enabled = false;
        }
        if(!hasPistol && !hasShotgun && pistolAmmo > 0 && shotgunAmmo<=0)
        {
            UIManager.UIM.ammoText.text = "Pistol ammo: " + pistolAmmo;
        }
        if (!hasPistol && !hasShotgun && pistolAmmo <= 0 && shotgunAmmo > 0)
        {
            UIManager.UIM.ammoText.text = "Shotgun ammo: " + shotgunAmmo;
        }
        if (!hasPistol && !hasShotgun && pistolAmmo > 0 && shotgunAmmo > 0)
        {
            //show both? (come back to this)
        }
        if (hasPistol)
        {
            
            PistolRay();
            UIManager.UIM.ammoText.enabled = true;
            UIManager.UIM.ammoText.text = "Pistol ammo: " + pistolAmmo;
            if (Input.GetMouseButton(1))
            {
                aiming = true;
            }
            else
            {
                aiming = false;
            }
            if (Input.GetMouseButtonUp(1))
            {
                transform.rotation = Quaternion.Euler(0, eAngleY, 0);
            }
            if (Input.GetMouseButtonDown(0) && pistolAmmo > 0)
            {
                ShootPistol();
            }
           
            
        }
        if (hasShotgun)
        {
           
            ShotgunRay();
            UIManager.UIM.ammoText.enabled = true;
            UIManager.UIM.ammoText.text = "Shotgun ammo: " + shotgunAmmo;
            if (Input.GetMouseButton(1))
            {
                aiming = true;
            }
            else
            {
                aiming = false;
            }
            if (Input.GetMouseButtonUp(1))
            {
                transform.rotation = Quaternion.Euler(0, eAngleY, 0);
            }
            if (Input.GetMouseButtonDown(0) && shotgunAmmo > 0)
            {
                ShootShotgun();
            }
           
        }
        if(!aiming)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, eAngleY, 0)), aimVertSpeed);

        }
        if (DialogueManager.DM.inDialogue)
        {
            UIManager.UIM.interactText.enabled = false;
            if (Input.GetButtonDown("Jump") && DialogueManager.DM.sentenceTyped)
            {
                DialogueManager.DM.DisplayNextSentence();
            }
            //if (Input.GetButtonDown("Jump") && DialogueManager.DM.sentenceTyped)
            //{
            //    DialogueManager.DM.DisplayNextSentence();
            //}
            else
            {

            }
        }

        
        

    }
}

