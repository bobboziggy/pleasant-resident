﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour {
    public static DialogueManager DM;
    public bool inDialogue, sentenceTyped;
    public float textSpeed;
    public Queue<string> sentences;

    private void Awake()
    {
        if (DM == null)
        {
            DM = this;
        }
        else
        {
            Destroy(gameObject);
        }
        sentences = new Queue<string>();
    }
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void StartDialogue(Dialogue d)

    {
        inDialogue = true;
        UIManager.UIM.diaAnim.SetBool("isOpen", true);
        //duringConvo = true;
        //print(d.NPCName);

       UIManager.UIM.nameTxt.text = d.NPCName;

        //clear sentences

        sentences.Clear();

        //loop through the dialogue's sentences array

        foreach (string s in d.mySentences)

        {

            //Enqueue each sentence:

            sentences.Enqueue(s);

        }

        DisplayNextSentence();
        //inDialogue = false;
    }



    public void DisplayNextSentence()

    {

        if (sentences.Count <= 0)

        {

            EndDialogue();
            //duringConvo = false;

        }

        else

        {
            
            

            //the following line straight up sets the text:
          /*  UIManager.UIM.convoTxt.text = sentences.Dequeue(); */// sets convotext

            //this line starts a coroutine that types it in:

            StartCoroutine(TypeSentence(sentences.Dequeue())); // types it out one letter at a time

            print(UIManager.UIM.convoTxt.text);
        }

    }



    public void EndDialogue()

    {

        UIManager.UIM.diaAnim.SetBool("isOpen", false);
        inDialogue = false;
        print("No More Sentences");

    }



    //OPTIONAL Coroutine to add dialogue char by char:

    IEnumerator TypeSentence(string sentenceToType)

    {
        sentenceTyped = false;
        UIManager.UIM.nextTextTxt.enabled = false;
        UIManager.UIM.convoTxt.text = "";

        foreach (char c in sentenceToType.ToCharArray())

        {

            UIManager.UIM.convoTxt.text += c;

            //yield return null;

            //orrrr if you want to have a variable text rate:

            yield return new WaitForSeconds(textSpeed);

        }
        sentenceTyped = true;
        UIManager.UIM.nextTextTxt.enabled = true;
    }
}

