﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour {
    //Rigidbody rb;
    public GameObject player;
    public GameObject room;
    public float enemyHP;
    public float damage;
    public float enemySpeed;
    public float tooFarNum;
    public bool chasingPlayer, hasChasedInPast,justHit,touchedDoor;
    NavMeshAgent agent;
    public Vector3 startPos;
    public Vector3 subtractBy;
    public float waitTime,doorTime,angSpeedDist;

    // Use this for initialization

    void Start()
    {

        //rb = GetComponent<Rigidbody>();

        agent = GetComponent<NavMeshAgent>();

        player = GameObject.FindGameObjectWithTag("Player");

        startPos = transform.position - subtractBy;

    }



    // Update is called once per frame

    void Update()
    {

        if (!DialogueManager.DM.inDialogue)

        {
            if (Vector3.Distance(transform.position, player.transform.position) >= angSpeedDist && !justHit)
            {
                agent.angularSpeed = 360;
            }
            if (enemyHP <= 0)

            {
                agent.enabled = false;
                Destroy(gameObject);

            }

            //ChasePlayer();

            if (chasingPlayer)

            {
                agent.stoppingDistance = 0;
                if (agent.enabled)
                {
                    agent.SetDestination(player.transform.position);
                }


                //rb.MovePosition(Vector3.MoveTowards(transform.position, player.transform.position, enemySpeed * Time.deltaTime));

                if (Vector3.Distance(transform.position, player.transform.position) > tooFarNum)

                {
                    if (agent.enabled)
                    {
                        agent.SetDestination(startPos);
                    }
                   
                   
                    //return to spawn postition

                    chasingPlayer = false;
                    
                    //maybe change speed? and make ignore doors?

                }

            }

            if (Vector3.Distance(transform.position, player.transform.position) <= tooFarNum && hasChasedInPast)

            {
                if (!justHit && !touchedDoor)
                {
                    chasingPlayer = true;
                }
                if (justHit || touchedDoor)
                {
                    chasingPlayer = false;
                    hasChasedInPast = false;
                }
                //return to spawn postition

                //chasingPlayer = true;

            }
            

            //if(room )

            //      {

            //          ChasePlayer();

            //      }

        }

        if (DialogueManager.DM.inDialogue)

        {

            if (enemyHP <= 0)

            {
                agent.enabled = false;
                Destroy(gameObject);

            }

        }





    }

    public void ChasePlayer()

    {

        if (room == ResidentMove.RM.roomForEnemy)

        {

            //rb.MovePosition(Vector3.MoveTowards(transform.position, player.transform.position, enemySpeed*Time.deltaTime));

            chasingPlayer = true;

            hasChasedInPast = true;

        }

    }

    public IEnumerator PlayerCollision()

    {
        

        chasingPlayer = false;
        justHit = true;
        agent.stoppingDistance = 2;
        if (agent.enabled)
        {
            agent.SetDestination(startPos);
        }
       
        agent.angularSpeed = 0;
        yield return new WaitForSeconds(waitTime);
       
        chasingPlayer = true;
        justHit = false;
        
        agent.stoppingDistance = 0;
        print("IN PLAYER COLLISION");
        
        
        //rb.isKinematic = true;

        //agent.enabled = true;

        //print("here");

        //agent.SetDestination(player.transform.position);

    }
    public IEnumerator DoorCollision()

    {
        chasingPlayer = false;
        touchedDoor = true;
        agent.isStopped = true;
        if (agent.enabled)
        {
            agent.SetDestination(startPos);
        }
        
        agent.stoppingDistance = 2;

        yield return new WaitForSeconds(doorTime);
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        yield return new WaitForSeconds(doorTime-1);
        touchedDoor = false;
        hasChasedInPast = false;

    }

    private void OnTriggerEnter(Collider other)

    {

        if (other.transform.CompareTag("Door"))

        {

            StartCoroutine(DoorCollision());

        }

    }

    private void OnCollisionEnter(Collision other)

    {

        if (other.transform.CompareTag("Player"))

        {

            DealDamage(damage);

            //rb.isKinematic = true;

            //agent.enabled = false;

            //agent.SetDestination(startPos);

            StartCoroutine(PlayerCollision());

        }

        if (other.transform.CompareTag("Door"))

        {

            StartCoroutine(DoorCollision());

        }

    }

    public void DealDamage(float dmgToDeal)

    {

        ResidentMove.RM.playerHP = ResidentMove.RM.playerHP - dmgToDeal;

    }

    public void TakeDamage(float dmgToTake)

    {

        enemyHP = enemyHP - dmgToTake;

    }

}