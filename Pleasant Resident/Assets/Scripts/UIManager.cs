﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public static UIManager UIM;

    [Header("UI Vars")]
    public Canvas mainCan;
    public TextMeshProUGUI interactText, ammoText,pickupText,keyText;
    public Button korpYes, korpNo,korbYes,korbNo;
    //public Image kpYImg, kpNImg, kbYImg, kbNImg;
    [Header("DialogueVars")]
    public TextMeshProUGUI nameTxt;
    public TextMeshProUGUI convoTxt;
    public TextMeshProUGUI nextTextTxt;

    public Image diaBoxImage;
    public GameObject dBox; //we're mostly going to use this to access its Animator
    public Animator diaAnim; //this is the dialogue box's animator component.

    private void Awake()
    {
        if(UIM == null)
        {
            UIM = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start () {
        //this works because we set dBox in the inspector.
        //otherwise we would have to get dBox first.
        mainCan.enabled = true;
        diaAnim = dBox.GetComponent<Animator>();
        diaBoxImage = dBox.GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
   
}
